# HTML and CSS Tools II

## Project 2 - Universitat Oberta de Catalunya

[Chakir Mrabet](mailto:cmrabet@gmail.com) - October 30, 2020,

# What is it?

This is a portal for a baroque festival celebrating the 333Th anniversary of **Jean-Baptiste Lully**. The website has been implemented using Parcel, Bootstrap, SASS, Stylelint, and the [H.Roberts style user guide](https://cssguidelin.es/).

# Where is it?

You can visit the site [here](https://lully-festival.netlify.app/).

# Instructions

For development, run the following command from the root folder:

`npm run dev`

For production:

`npm run build`

The files for production will be in the `dist` folder.
