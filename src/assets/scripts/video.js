/**
 * Herramientas HTML y CSS II - PEC 2.
 * Chakir Mrabet, 2020.
 * Lazy loading of YouTube videos.
 */

/**
 * Functionality.
 *
 * 1. Attaches a click event to the document, and checks if an img element
 *    with the js-video class is the trigger, and if so, it replaces it
 *    with an iframe element setting its source to the img's data-video
 *    attribute.
 * 2. The videos will start playing automatically.
 * 3. Only one video can play at a time.
 */

// We use the HTML5 YouTube API to control the videos.

function stopAllVideos() {
  const frames = document.querySelectorAll("iframe.js-iframe-video");

  frames.forEach((frame) => {
    frame.contentWindow.postMessage(
      JSON.stringify({ event: "command", func: "stopVideo" }),
      "*"
    );
  });
}

export default function VideoControl() {
  document.addEventListener("click", (e) => {
    let item = e.target;

    if (item.classList.contains("flex-catalog__item")) {
      item = item.querySelector("img");
    }

    if (item.classList.contains("js-video")) {
      const videoUrl = item.dataset.video;

      if (videoUrl) {
        // We don't want more than one video playing at a time.
        // Before we play a new one, we stop all.

        stopAllVideos();

        // Create a new frame that will host the video.

        const frame = document.createElement("iframe");
        frame.classList.add(
          "flex-catalog__item-image",
          "flex-catalog__item--is-frame",
          "js-iframe-video"
        );

        // The autoplay is key to make the video to start
        // by itself after the iframe element is rendered.

        frame.src = videoUrl + "?autoplay=1&version=3&enablejsapi=1";
        frame.allow = "autoplay";

        // Replace the img element with the new iframe one.

        item.outerHTML = frame.outerHTML;
      }
    }
  });
}
