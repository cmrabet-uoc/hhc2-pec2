/**
 * Herramientas HTML y CSS II - PEC 2.
 * Chakir Mrabet, 2020.
 * Script to manage the functionality of the contact us form.
 */

/**
 * Submits the values of the form to the 3rd party service
 * FormSpree using Fetch.
 *
 * 1. While submitting, it hides the container that has the form
 *    and contact information. It also shows a loader.
 * 2. When the submission is finished, it shows a thank you message
 *    for 5 seconds before showing again the container that has the
 *    form and contact information.
 * 3. If there is a problem with the submission, an alert box will
 *    inform the user.
 */

const FORM_API = "https://formspree.io/f/xbjpakpk";

export default function ContactFormControl() {
  const formLoaderEl = document.querySelector(".js-contact-form-loader");
  const thanksTextEl = document.querySelector(".js-contact-form-thanks");
  const formEl = document.querySelector(".js-contact-form");

  if (formEl) {
    formEl.addEventListener("submit", async (e) => {
      e.preventDefault();

      // We use the HTML5 constraint validation API to
      // validate the form.
      if (!formEl.checkValidity()) {
        formEl.classList.add("was-validated");
        return;
      }

      show(formEl, false);
      show(formLoaderEl, true);

      const body = new FormData(formEl);

      try {
        const res = await fetch(FORM_API, {
          method: "post",
          headers: {
            Accept: "application/json",
          },
          body,
        });

        if (res.ok) {
          formEl.reset();

          show(formLoaderEl, false);
          show(thanksTextEl, true);

          setTimeout(() => {
            show(thanksTextEl, false);
            formEl.classList.remove("was-validated");
            show(formEl, true);
          }, 5000);
          return;
        }

        showError(`Your request was not submitted. ${res.statusText}`);
      } catch (e) {
        showError(
          "There was an error when submitting your request, please try again later."
        );
      }

      show(thanksTextEl, false);
      show(formEl, true);
    });
  }
}

function show(el, isVisible) {
  if (isVisible) {
    el.classList.remove("d-none");
    return;
  }

  el.classList.add("d-none");
}

function showError(msg) {
  alert(msg);
}
