/**
 * Herramientas HTML y CSS II - PEC 2.
 * Chakir Mrabet, 2020.
 * Entry script.
 */

/**
 * This is to fix the issue explained here: https://flaviocopes.com/parcel-regeneratorruntime-not-defined/
 * This is an issue with Parcel.
 */

import "regenerator-runtime/runtime";

/**
 * Bootstrap Library.
 */

import "bootstrap";

/**
 * Custom made modules.
 */

import nav from "./nav";
import video from "./video.js";
import contact from "./contact";
import map from "./map";

/**
 * Navbar functionality.
 */

nav();

/**
 * Video Control
 */

video();

/**
 * Contact form functionality.
 */

contact();

/**
 * Venues map functionality.
 */

map();
