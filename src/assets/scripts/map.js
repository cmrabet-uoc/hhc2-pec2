/**
 * Herramientas HTML y CSS II - PEC 2.
 * Chakir Mrabet, 2020.
 * Venues Map script.
 */

/**
 * Functionality.
 *
 * 1. Shows a map with the locations of the venues where the festival
 *    is taking place, using the Mapbox API (https://docs.mapbox.com/).
 * 2. Create a map centered in Lynchburg and adds 3 markers with a name.
 */

const venues = [
  { name: "Academy Center of Arts", lat: 37.41763, lng: -79.14393 },
  { name: "Opera on the James", lat: 37.41653, lng: -79.14378 },
  { name: "City Auditorium", lat: 37.41305, lng: -79.14026 },
];

export default function MapControl() {
  if (window.mapboxgl === undefined) {
    return;
  }

  mapboxgl.accessToken =
    "pk.eyJ1IjoiY21yYWJldCIsImEiOiJjajhvc3hleTEwN3N3MnFxY2o5Znh6NnRuIn0.BeXGMYcmiAs_rQjw9C_64Q";

  // Create a new Map centered in Lynchburg.

  const map = new mapboxgl.Map({
    container: "venues-map",
    style: "mapbox://styles/mapbox/light-v10",
    center: [-79.145, 37.4156883],
    zoom: 14.5,
  });

  // Add markers to the map

  venues.forEach((venue) => {
    const popup = new mapboxgl.Popup({
      offset: 25,
      closeButton: false,
      className: "map__popup",
    }).setText(venue.name);

    new mapboxgl.Marker({ color: "red" })
      .setLngLat([venue.lng, venue.lat])
      .setPopup(popup)
      .addTo(map);
  });
}
